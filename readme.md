# Patiente Application
This application load data to db collection ant test if it's ok:

 - Load data from flat file to Mongo database.
 - Test data in database.

## Installation
1. Install all dependencies - npm install.
2. Create variables.env file in the root folder with CSV="./data/patients.csv" - path to file to parse and  MONGO field with MongoDb address and start your Mongo database server.
3. Load data into testing database.
3. Start testing.

## Usage
To load data from flat file to database
```sh
$ npm start
```
To start test framework
```sh
$ npm run test
```
