'use strict';

require('dotenv').config();
const fs = require('fs');
const mongoose = require('mongoose');
const parser = require('./lib/parser');
const csvFileName = process.env.CSV;
const databaseUri = process.env.MONGO;
const connectOptions = {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true
};


const connectDB = async () => {
	mongoose.connection.on("connected", async () => {
		console.log("MONGOOSE CONNECTED!!");
		await loadDataToDb();
	});

	mongoose.connection.on('error', (error) => {
		console.error(`Fail connect to ${databaseUri}.\n Error: ${error.message}`);
		throw new Error(`Couldn't connect to ${databaseUri} database. Error: ${error.message}`);
	});

	await mongoose
		.connect(databaseUri, connectOptions)
		.catch(error => {
			console.error(error);
			throw error;
		});
}

async function loadDataToDb() {
	await fs.access(csvFileName, fs.constants.F_OK, (err) => {
		if(err){
			console.error(`File ${csvFileName} does not exist.`);
			throw err;
		}

		console.log(`File ${csvFileName} exists.`);
		parser(
			csvFileName
		)
		.then(result => {
			console.log('Data loaded', result);
		}).catch((error => {
			console.log('Loading error', error);
			process.exit(0);
		}));
	});
  }
  


process.on('SIGINT', () => {
	mongoose.connection.close(() => {
		console.info('Mongoose is disconnected due to application termination.');
	});
});

connectDB();