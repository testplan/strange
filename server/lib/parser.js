'use strict';

const fs = require('fs');
const mongoose = require('mongoose');

const csv = require('csv-parser');
const moment = require('moment');
const Email = require('../models/Email');
const Patient = require('../models/Patient');
const numberOfEmails = 4;

function camelize(str) {
    const camelCase = str => {
        let string = str.toLowerCase().replace(/[^A-Za-z0-9]/g, ' ').split(' ')
            .reduce((result, word) => result + capitalize(word.toLowerCase()))
        return string.charAt(0).toLowerCase() + string.slice(1)
    }

    const capitalize = str => str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
    return camelCase(str);
}

const createEmails = async (patients) => {
    let addEmails = [];
    if (patients[0]) {
        if (patients[0]._id) {
            const res = await Patient.findById(patients[0]._id);
            if (res && res.consent === 'Y') {
                for (let i = 1; i <= numberOfEmails; i++) {
                    let newEmail = new Email({
                        id: res._id,
                        name: 'Day ' + i,
                        scheduled_date: moment().add(i, 'days')
                    });
                    const sendEmail = await newEmail.save();
                    addEmails.push(sendEmail);
                }
            }
            return addEmails;
        }
    }
}

const populatePatientEmails = async (createdEmails) => {
    const emails = createdEmails.map(item=>{
        return item._id
    });
    const mongoObjectId = mongoose.Types.ObjectId(createdEmails[0].id);
    await Patient.updateMany({_id: mongoObjectId}, { $set: {emails: emails}});
}

const parser = async function (fileName) {

    if (!fileName) {
        return
    }
    // await Patient.deleteMany();
    // await Email.deleteMany();
    return new Promise((resolve, reject) => {
        let headerRow;

        fs.createReadStream(fileName)
            .pipe(csv({
                separator: '|'
            }))
            .on('headers', async (headers) => {
                headerRow = headers.map((item) => {
                    return {
                        item,
                        name: camelize(item)
                    };
                });
            })
            .on('data', (data) => {
                let patientRow = {};
                headerRow.map(item => {
                    patientRow[item.name] = data[item.item] || ''
                })

                let newPatient = {
                    updateOne: {
                        filter: {
                            memberId: patientRow.memberId
                        },
                        update: patientRow,
                        upsert: true
                    }
                };

                Patient.bulkWrite([newPatient], async (err, res) => {
                    if (res.result.upserted.length) {
                        const emails = await createEmails(res.result.upserted);
                        if (emails.length) {
                            await populatePatientEmails(emails);
                        }
                    }
                })
            })
            .on('end', () => {
                resolve('ok')
            })
            .on('error', (error) => {
                reject(`Error in the read stream ${error}`);
            });
    });
}

module.exports = parser;