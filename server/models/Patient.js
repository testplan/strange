'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const patientSchema = new Schema(
	{
		programIdentifier: String,
		dataSource: String,
		cardNumber: String,
		memberId: String,
		firstName: String,
		lastName: String,
		dateOfBirth: String,
		address1: String,
		address2: String,
		city: String,
		state: String,
		zipCode: String,
		telephoneNumber: String,
		emailAddress: String,
		consent: String,
		mobilePhone: String,
		emails: [{type: Schema.Types.ObjectId, ref: 'Email'}]
	},
	{
		timestamps: true,
		versionKey: false
	}
);

const Patient = mongoose.model('Patient', patientSchema);

module.exports = Patient;