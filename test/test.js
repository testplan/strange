'use strict';
/* eslint-disable no-unused-expressions, no-unused-vars, func-names, prefer-arrow-callback */
const fs = require('fs');
require('dotenv').config();

const Promise = require('bluebird');

const chai = require('chai');
const expect = chai.expect;

const csv = require('csv-parser');
const moment = require('moment');

const Email = require('../server/models/Email');
const Patient = require('../server/models/Patient')

const mongoose = require('mongoose');

const csvFileName = process.env.CSV;
const databaseUri = process.env.MONGO;

const connectOptions = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
};

const specialSeparator = '|';
function camelize(str) {
    const camelCase = str => {
        let string = str.toLowerCase().replace(/[^A-Za-z0-9]/g, ' ').split(' ')
            .reduce((result, word) => result + capitalize(word.toLowerCase()))
        return string.charAt(0).toLowerCase() + string.slice(1)
    }

    const capitalize = str => str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
    return camelCase(str);
}

describe('Test data loading', function() {
    let headerRow;
    let db;
    let countRows;
    let rows;

    before(async function() {
        countRows = 0;
        rows = [];
        await mongoose.connect(databaseUri, connectOptions, (err, res) => {
            if (err) {
                console.log('error', err);
                process.exit(1);
            }
        });
        db = mongoose.connection;
    });

    after(function() {
        if(db) {
            db.close();
        }
    });

    it('Data in CSV should match Patients collection data', function(done) {
        let stream = fs.createReadStream(csvFileName);
        let csvStream = csv({separator: specialSeparator})
            .on('headers', (headers) => {
                headerRow = headers.map((item) => {
                    return {
                        item,
                        name: camelize(item)
                    };
                });
            })
            .on('data', async (data) => {
                countRows++
                let patientRow = {};
                headerRow.map(item => {
                    patientRow[item.name] = data[item.item] || ''
                })
                rows.push(patientRow);
                const row = await Patient.exists(patientRow);
                expect(row).to.be.true;
                rows = [];
            })
            .on('end', async () => {
                if(countRows){
                    return Promise.all(rows.map(row => {
                        return Patient.exists(row).catch((error => {
                            done(error);
                        }));
                    })).then((result) => {
                        const r = result.reduce((accumulator, currentValue) => accumulator && currentValue)
                        expect(r).to.be.true;
                        rows = [];
                        done();
                    }).catch(error => {
                        done(error);
                    });
                }
            })
            .on('error', (error) => {
                console.log('error', `Error in the read stream ${error} `);
            });

        stream.pipe(csvStream);
    });

    it('Patient IDs with first name missing', function(done) {
        Patient.find({firstName: ''}).then(result => {
            expect(result).to.be.an('array');
            result.forEach(value => {
               expect(value.firstName).to.be.eql('');
            });
            console.log(`Patient IDs with first name missing:`);
            console.log(result.map(item=>item._id ).join(', '));
            done();
        })
        .catch((error => {
            done(error);
        }))
    });

    it('Patient IDs with missing email and consent set to Y', function(done) {
        Patient.find({emailAddress: '', consent: 'Y'}).then(result => {
            expect(result).to.be.an('array');
            result.forEach(value => {
                expect(value.emailAddress).to.be.eql('');
                expect(value.consent).to.be.eql('Y');
            });
            console.log(`Patient IDs with missing email and consent set to Y:`);
            console.log(result.map(item=>item._id ).join(', '));
            done();
        })
        .catch((error => {
            done(error);
        }));
    });

    it('Verify Emails in Emails Collection for patients who have CONSENT as Y', function(done) {
        Patient
            .find({consent: 'Y'})
            .populate([{
                path:'emails',
                model:'Email'
            }])
            .exec()
            .then((data) => {
                expect(data).to.be.an('array');
                expect(data.length).to.be.at.least(1);
                data.forEach(patient => {
                    expect(patient.consent).to.be.eql('Y');
                    expect(patient.emails).to.be.an('array');
                    expect(patient.emails).to.have.length(4);
                });
                done();
            })
            .catch((error => {
                done(error);
            }));
    });

    it('Verify scheduled emails for each patient', function(done) {
        Patient
            .find({consent: 'Y'})
            .populate([{
                path:'emails',
                model:'Email'
            }])
            .exec()
            .then((data) => {
                data.forEach(patient => {
                    let whenScheduled = [];
                    let i = 1;
                    patient.emails.forEach(email => {
                        expect(email.id).to.be.eql(patient._id);
                        expect(email.name).to.be.eql('Day ' + i);
                        whenScheduled
                            .push(moment(new Date(email.scheduled_date))
                            .subtract(i, 'days')
                            .format('YYYY-MM-DD'));

                        i++;
                    })
                    const r = whenScheduled.reduce(function(prev, cur) {
                        return (prev.indexOf(cur) < 0) ? prev.concat([cur]) : prev;
                       }, []);

                    expect(r.length).to.be.eql(1);
                });
                done();
            })
            .catch((error => {
                done(error);
            }));
    });
});